package observers;

import server.ServerFrame.Server;
import client.Client;
import dbOperations.BookOperations;

public class ClientObserver extends Thread{

	private Server server;
	private Client client;
	
	public ClientObserver(Server server, Client client){
		this.server = server;
		this.client = client;
	}
	

	@Override
	public void run() {
		while(!client.observedAction()){
			try {
				client.wait();
				server.updateClients();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
