package dbOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

import model.Book;

public class BookOperations {

	
	public ArrayList<Book> readBookXML() {
		ArrayList<Book> listBooks = new ArrayList<>();
		try {
			String filePath = "src/xml/book.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(fileInputStream);

			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				
				Integer id = staff.getAttribute("id").getIntValue();
				List<Element> noduri = staff.getChildren();
				String title = noduri.get(0).getValue();
				String author = noduri.get(1).getValue();
				int year = Integer.parseInt(noduri.get(2).getValue());
				int quantity = Integer.parseInt(noduri.get(3).getValue());
				int price = Integer.parseInt(noduri.get(4).getValue());
			
				Book book = new Book(id, title, author, quantity, price, year);
				
				listBooks.add(book);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return listBooks;
	}
	
	public void deleteBook(int id) {
		try {
			String filePath = "src/xml/book.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(fileInputStream);

			Element bookstore = document.getRootElement();
			List<Element> staffs = bookstore.getChildren();
			for (int i = 0; i < staffs.size() - 1; i++) {
				Element staff = staffs.get(i);
				Integer idd = staff.getAttribute("id").getIntValue();
				if (idd.equals(id)) {
					bookstore.removeContent(staff);
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(document, new FileOutputStream(new File(absolutePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	
	public void insertBook(Book book) throws SAXException {
		try {

			String filePath = "src/xml/book.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element company = doc.getRootElement();
			Element bookElement = new Element("book");
			bookElement.setAttribute("id", book.getId() + "");

			Element title = new Element("title");
			title.addContent(new Text(book.getTitle()));
			Element author = new Element("author");
			author.addContent(new Text(book.getAuthor()));
			Element year = new Element("year");
			year.addContent(new Text(book.getYear() + ""));
			Element quantity = new Element("quantity");
			quantity.addContent(new Text(book.getQuantity() + ""));
			Element price = new Element("price");
			price.addContent(new Text(book.getPrice() + ""));

			bookElement.addContent(title);
			bookElement.addContent(author);
			bookElement.addContent(year);
			bookElement.addContent(quantity);
			bookElement.addContent(price);
			company.addContent(bookElement);

			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(absolutePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Book> searchBooks(String stringRegex) {
		ArrayList<Book> booksList = new ArrayList<>();
		try {
			String filePath = "src/xml/book.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element bookElement = books.get(i);
				List<Element> noduri = bookElement.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ( ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().matches(stringRegex)) ||
							("author".equals(noduri.get(j).getName()) && noduri.get(j).getValue().matches(stringRegex)) ||
							("year".equals(noduri.get(j).getName()) && noduri.get(j).getValue().matches(stringRegex)) ){
						Element cautat = bookElement;

						Integer id = bookElement.getAttribute("id").getIntValue();
						List<Element> nodurile = cautat.getChildren();
						String title = nodurile.get(0).getValue();
						String author = nodurile.get(1).getValue();
						int year = Integer.parseInt(nodurile.get(2).getValue());
						int quantity = Integer.parseInt(nodurile.get(3).getValue());
						int price = Integer.parseInt(nodurile.get(4).getValue());
						
						Book book = new Book(id, title, author, quantity, price, year);
						
						boolean found = false;
						
						for(Book bookIterator : booksList){
							if(bookIterator.getTitle().equals(book.getTitle()))
								found = true;	
						}
						
						if(!found){
							booksList.add(book);
						}
						
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return booksList;
	}


	public void updateBook(Book book) {
		try {
			String filePath = "src/xml/book.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();

			Document document = saxBuilder.build(fileInputStream);
			Element bookstore = document.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element bookElement = staffs.get(i);
				List<Element> element = bookElement.getChildren();
				if (bookElement.getAttribute("id").getIntValue()== book.getId()){
					for (int j = 0; j < element.size()-1; j++) {
						if (!element.get(j).getValue().equals(book.getAttr(element.get(j).getName()))){
							element.get(j).removeContent();
							element.get(j).addContent( book.getAttr(element.get(j).getName()) );
						}
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(document, new FileOutputStream(new File(absolutePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void writeReport(ArrayList<Book> books) {
		try {
			String filePath = "src/xml/report.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();

			Document document = saxBuilder.build(fileInputStream);
			
			Element bookstore = document.getRootElement();
			bookstore.removeContent();
			
			for(Book book: books){
				if(book.getQuantity() == 0){
					Element bookElement = new Element("book");
					bookElement.setAttribute("id", book.getId() + "");
		
					Element title = new Element("title");
					title.addContent(new Text(book.getTitle()));
					Element author = new Element("author");
					author.addContent(new Text(book.getAuthor()));
					Element year = new Element("year");
					year.addContent(new Text(book.getYear() + ""));
					Element quantity = new Element("quantity");
					quantity.addContent(new Text(book.getQuantity() + ""));
					Element price = new Element("price");
					price.addContent(new Text(book.getPrice() + ""));
		
					bookElement.addContent(title);
					bookElement.addContent(author);
					bookElement.addContent(year);
					bookElement.addContent(quantity);
					bookElement.addContent(price);
					bookstore.addContent(bookElement);
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(document, new FileOutputStream(new File(absolutePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
}
