package view;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;

import model.Book;
import model.User;

public class AdminView extends JFrame{
	
	private JPanel panel;
	private JLabel welcome;
	
	private JTable booksTable;
	private JTable usersTable;
	
	private JRadioButton optionUser;
	private JRadioButton optionBook;
	private ButtonGroup groupModel;
	private JButton switchModelButton;
	
	private JRadioButton optionCreate;
	private JRadioButton optionDelete;
	private JRadioButton optionUpdate;
	private ButtonGroup groupCRUD;
	private JButton switchCRUDButton;
	
	private JLabel idLabel;
	
	private JLabel titleLabel;
	private JLabel authorLabel;
	private JLabel yearLabel;
	private JLabel quantityLabel;
	private JLabel priceLabel;
	
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JLabel is_staffLabel;
	private JLabel emailLabel;
	
	private JTextField idField;
	
	private JTextField titleField;
	private JTextField authorField;
	private JTextField yearField;
	private JTextField quantityField;
	private JTextField priceField;

	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField usernameField;
	private JTextField passwordField;
	private JTextField is_staffField;
	private JTextField emailField;
	
	private JButton execute;
	
	private JButton report;
	
	public AdminView(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("StoreBook(admin)");
		this.setSize(480, 800);
		
		this.panel = new JPanel();

		this.welcome = new JLabel("");
		this.panel.add(welcome);
		
		this.report = new JButton("report");
		
		this.optionUser= new JRadioButton("USER");
		this.optionBook= new JRadioButton("BOOK");
		this.optionUser.setSelected(true);
		this.groupModel = new ButtonGroup();
		this.switchModelButton = new JButton("GO");
		
		this.optionCreate = new JRadioButton("Create");
		this.optionDelete = new JRadioButton("Delete");
		this.optionUpdate = new JRadioButton("Update");
		this.optionCreate.setSelected(true);
		this.groupCRUD = new ButtonGroup();
		this.switchCRUDButton = new JButton("GO");
		this.execute = new JButton("execute");
		
		this.idLabel = new JLabel("id");
		
		this.titleLabel = new JLabel("Title");
		this.authorLabel = new JLabel("Author");
		this.yearLabel = new JLabel("Year");
		this.quantityLabel = new JLabel("Quantity");
		this.priceLabel = new JLabel("Price");
		
		this.firstNameLabel = new JLabel("First Name");
		this.lastNameLabel = new JLabel("Last Name");
		this.usernameLabel = new JLabel("Username");
		this.passwordLabel = new JLabel("Password");
		this.is_staffLabel = new JLabel("is_staff");
		this.emailLabel = new JLabel("email");
		
		this.idField = new JTextField("", 15);
		
		this.titleField = new JTextField("", 15);
		this.authorField = new JTextField("", 15);
		this.yearField = new JTextField("", 15);
		this.quantityField = new JTextField("", 15);
		this.priceField = new JTextField("", 15);
		
		this.firstNameField = new JTextField("", 15);
		this.lastNameField = new JTextField("", 15);
		this.usernameField = new JTextField("", 15);
		this.passwordField = new JTextField("", 15);
		this.is_staffField = new JTextField("", 15);
		this.emailField = new JTextField("", 15);
		
		
		
		this.add(panel);
		
		this.setVisible(true);
	}
	
	public void switchModelButtonListener(ActionListener listenForButton) {
		switchModelButton.addActionListener(listenForButton);
	}
	public void reportButtonListener(ActionListener listenForButton) {
		report.addActionListener(listenForButton);
	}
	public void switchCRUDButtonListener(ActionListener listenForButton) {
		switchCRUDButton.addActionListener(listenForButton);
	}
	public void executeButtonListener(ActionListener listenForButton) {
		execute.addActionListener(listenForButton);
	}
	
	public JLabel getWelcome() {
		return welcome;
	}

	public void setWelcome(JLabel welcome) {
		this.welcome = welcome;
	}

	public JTable getBooksTable() {
		return booksTable;
	}

	public JRadioButton getOptionCreate() {
		return optionCreate;
	}

	public void setOptionCreate(JRadioButton optionCreate) {
		this.optionCreate = optionCreate;
	}

	public JRadioButton getOptionDelete() {
		return optionDelete;
	}

	public void setOptionDelete(JRadioButton optionDelete) {
		this.optionDelete = optionDelete;
	}

	public JRadioButton getOptionUpdate() {
		return optionUpdate;
	}

	public void setOptionUpdate(JRadioButton optionUpdate) {
		this.optionUpdate = optionUpdate;
	}
	
	public JTextField getIdField() {
		return idField;
	}

	public JTextField getTitleField() {
		return titleField;
	}

	public JTextField getAuthorField() {
		return authorField;
	}

	public JTextField getYearField() {
		return yearField;
	}

	public JTextField getQuantityField() {
		return quantityField;
	}

	public JTextField getPriceField() {
		return priceField;
	}

	public JTextField getFirstNameField() {
		return firstNameField;
	}

	public JTextField getLastNameField() {
		return lastNameField;
	}

	public JTextField getUsernameField() {
		return usernameField;
	}

	public JTextField getPasswordField() {
		return passwordField;
	}

	public JTextField getIs_staffField() {
		return is_staffField;
	}

	public JTextField getEmailField() {
		return emailField;
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(null, errorMessage);
	}
	
	public void setBooksTable(JTable booksTable) {
		this.panel.removeAll();	
		this.panel.add(welcome);
		this.panel.add(new JLabel("<html><br><br><br><br></html>"));
		
		
		this.optionBook.setSelected(true);
		this.groupModel.add(optionUser);
		this.groupModel.add(optionBook);
		this.optionBook.setSelected(true);
		this.panel.add(optionBook);
		this.panel.add(optionUser);
		this.panel.add(switchModelButton);
		this.panel.add(report);
		this.booksTable = booksTable;
		this.panel.add(new JScrollPane(booksTable));
	}

	public JTable getUsersTable() {
		return usersTable;
	}

	public void setUsersTable(JTable usersTable) {
		this.panel.removeAll();
		this.panel.add(welcome);
		this.panel.add(new JLabel("<html><br><br><br><br></html>"));
		
		
		this.optionUser.setSelected(true);
		this.groupModel.add(optionUser);
		this.groupModel.add(optionBook);
		this.panel.add(optionBook);
		this.panel.add(optionUser);
		this.panel.add(switchModelButton);
		
		this.usersTable = usersTable;
		this.panel.add(new JScrollPane(usersTable));
	}
	
	public void setCRUDPanel(String button){
		this.optionCreate.setSelected(false);
		this.optionUpdate.setSelected(false);
		this.optionDelete.setSelected(false);
		this.panel.add(optionCreate);
		this.panel.add(optionUpdate);
		this.panel.add(optionDelete);
		this.groupCRUD.add(optionCreate);
		this.groupCRUD.add(optionUpdate);
		this.groupCRUD.add(optionDelete);
		this.panel.add(switchCRUDButton);
		
		switch(button){
		case "create":this.optionCreate.setSelected(true);
			if(this.optionUser.isSelected()){
				this.panel.add(idLabel);
				this.panel.add(idField);
				this.panel.add(firstNameLabel);
				this.panel.add(firstNameField);
				this.panel.add(lastNameLabel);
				this.panel.add(lastNameField);
				this.panel.add(usernameLabel);
				this.panel.add(usernameField);
				this.panel.add(is_staffLabel);
				this.panel.add(is_staffField);
				this.panel.add(emailLabel);
				this.panel.add(emailField);
			}else{
				this.panel.add(idLabel);
				this.panel.add(idField);
				this.panel.add(titleLabel);
				this.panel.add(titleField);
				this.panel.add(authorLabel);
				this.panel.add(authorField);
				this.panel.add(yearLabel);
				this.panel.add(yearField);
				this.panel.add(quantityLabel);
				this.panel.add(quantityField);
				this.panel.add(priceLabel);
				this.panel.add(priceField);
			}
			break;
		case "update":this.optionUpdate.setSelected(true);
			if(this.optionUser.isSelected()){
				this.panel.add(idLabel);
				this.panel.add(idField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(firstNameLabel);
				this.panel.add(firstNameField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(lastNameLabel);
				this.panel.add(lastNameField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(usernameLabel);
				this.panel.add(usernameField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(is_staffLabel);
				this.panel.add(is_staffField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(emailLabel);
				this.panel.add(emailField);
			}else{
				this.panel.add(idLabel);
				this.panel.add(idField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(titleLabel);
				this.panel.add(titleField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(authorLabel);
				this.panel.add(authorField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(yearLabel);
				this.panel.add(yearField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(quantityLabel);
				this.panel.add(quantityField);
				this.panel.add(new JLabel("<html><br></html>"));
				this.panel.add(priceLabel);
				this.panel.add(priceField);
			}
			break;
		case "delete": this.optionDelete.setSelected(true);
			this.panel.add(idLabel);
			this.panel.add(idField);
			break;
		}
		this.panel.add(execute);
	}
	
	public void repaintPanel(){
		this.panel.repaint();
		this.panel.revalidate();
	}

	public JRadioButton getOption1() {
		return optionUser;
	}


	public void setOption1(boolean b) {
		this.optionUser.setSelected(b);
	}


	public JRadioButton getOption2() {
		return optionBook;
	}


	public void setOption2(boolean b) {
		this.optionBook.setSelected(b);
	}


	public JTable buildBooksTable(ArrayList<Book> books){
		 String columnNames[] = {"id", "title", "author", "year", "quantity", "price"};
		   Object[][] data = new Object[books.size()][6];

		   
		   	int i=0;
		   	while(i<books.size()){
		      for(Book book : books){
		    	 data[i][0] = book.getId();
		         data[i][1] = book.getTitle();
		         data[i][2] = book.getAuthor();
		         data[i][3] = book.getYear();
		         data[i][4] = book.getQuantity();
			     data[i][5] = book.getPrice();
		         i++;
		      }
		   	}

		   JTable table = new JTable(data, columnNames);
	
		   TableColumn columnTitle = table.getColumn("title");
		   TableColumn columnAuthor = table.getColumn("author");
		   TableColumn columnYear = table.getColumn("year");
		   TableColumn columnPrice= table.getColumn("price");
		   TableColumn columnQuantity = table.getColumn("quantity");
		   
		   columnTitle.setPreferredWidth(200);
		   columnAuthor.setPreferredWidth(100);
		   columnYear.setPreferredWidth(100);
		   columnPrice.setPreferredWidth(100);
		   columnQuantity.setPreferredWidth(100);
		   
		   return table;
	}
	
	public JTable buildUsersTable(ArrayList<User> users){
		 String columnNames[] = {"id", "firstname", "lastname", "username", "password", "is_staff", "email"};
		   Object[][] data = new Object[users.size()][7];

		   
		   	int i=0;
		   	while(i<users.size()){
		      for(User user : users){
		    	 data[i][0] = user.getId();
		         data[i][1] = user.getFirstname();
		         data[i][2] = user.getLastname();
		         data[i][3] = user.getUsername();
		         data[i][4] = user.getPassword();
			     data[i][5] = user.getIs_staff();
			     data[i][6] = user.getEmail();
		         i++;
		      }
		   	}

		   JTable table = new JTable(data, columnNames);
	
		   return table;
	}
}
