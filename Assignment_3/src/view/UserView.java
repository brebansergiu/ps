package view;


import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;

import dbOperations.BookOperations;

import model.Book;
import model.Store;

public class UserView extends JFrame{
	private BookOperations bookOperations;
	private JPanel panel;
	private JLabel welcome;
	
	private JLabel searchLabel;
	private JTextField searchTextField;
	
	private JButton searchButton;
	
	private JTable booksTable;
	
	private JLabel idLabel;
	private JTextField idTextField;
	private JLabel quantityLabel;
	private JTextField quatityTextField;
	private JButton sellButton;
	
	public UserView(BookOperations bookOperations){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("StoreBook(regular user)");
		this.bookOperations = bookOperations;
		
		this.setSize(500, 800);
		this.panel = new JPanel();

		this.welcome = new JLabel("");
		this.panel.add(welcome);
		
		this.panel.add(new JLabel("<html><br><br><br><br></html>"));
		
		this.searchLabel = new JLabel("search title, author or year( you can use reegular expresion): ");
		this.searchTextField = new JTextField("", 15);
		this.searchButton = new JButton("Search");
		
		this.panel.add(searchLabel);
		this.panel.add(searchTextField);
		this.panel.add(searchButton);
		
		booksTable = buildBooksTable(null);		
		this.panel.add(booksTable);
		this.panel.add(new JScrollPane(booksTable));
		
		this.idLabel = new JLabel("Book id:");
		this.idTextField = new JTextField("", 5);
		this.quantityLabel = new JLabel("quantity:");
		this.quatityTextField = new JTextField("",5);
		this.sellButton = new JButton("sell");
		
		this.panel.add(idLabel);
		this.panel.add(idTextField);
		this.panel.add(quantityLabel);
		this.panel.add(quatityTextField);
		this.panel.add(sellButton);
		
		this.add(panel);
	
		this.setVisible(true);
	}
	
	public void setWelcomeText(String username){
		this.welcome.setText("welcome in your account   " + username);
	}
	
	public JTable buildBooksTable(ArrayList<Book> books){
		if (books == null){
			books = bookOperations.readBookXML();	
		}
		 String columnNames[] = {"id", "title", "author", "year", "quantity", "price"};
		   Object[][] data = new Object[books.size()][6];

		   
		   	int i=0;
		   	while(i<books.size()){
		      for(Book book : books){
		    	 data[i][0] = book.getId();
		         data[i][1] = book.getTitle();
		         data[i][2] = book.getAuthor();
		         data[i][3] = book.getYear();
		         data[i][4] = book.getQuantity();
			     data[i][5] = book.getPrice();
		         i++;
		      }
		   	}

		   JTable table = new JTable(data, columnNames);
	
		   TableColumn columnTitle = table.getColumn("title");
		   TableColumn columnAuthor = table.getColumn("author");
		   TableColumn columnYear = table.getColumn("year");
		   TableColumn columnPrice= table.getColumn("price");
		   TableColumn columnQuantity = table.getColumn("quantity");
		   
		   columnTitle.setPreferredWidth(200);
		   columnAuthor.setPreferredWidth(100);
		   columnYear.setPreferredWidth(100);
		   columnPrice.setPreferredWidth(100);
		   columnQuantity.setPreferredWidth(100);
		   
		   return table;
	}
	
	public JTextField getSearchTextField() {
		return searchTextField;
	}
	
	public JTable getBooksTable() {
		return booksTable;
	}

	public JTextField getIdTextField() {
		return idTextField;
	}

	public void setIdTextField(JTextField idTextField) {
		this.idTextField = idTextField;
	}

	public JTextField getQuatityTextField() {
		return quatityTextField;
	}

	public void setQuatityTextField(JTextField quatityTextField) {
		this.quatityTextField = quatityTextField;
	}

	public void setBooksTable(JTable booksTable) {
		this.panel.removeAll();
		

		this.panel.add(welcome);
		
		this.panel.add(new JLabel("<html><br><br><br><br></html>"));
		

		this.panel.add(searchLabel);
		this.panel.add(searchTextField);
		this.panel.add(searchButton);
		
		this.booksTable = booksTable;
		this.panel.add(new JScrollPane(booksTable));
		
		this.idLabel = new JLabel("Book id:");
		this.idTextField = new JTextField("", 5);
		this.quantityLabel = new JLabel("quantity:");
		this.quatityTextField = new JTextField("",5);
		this.sellButton = new JButton("sell");
		
		this.panel.add(idLabel);
		this.panel.add(idTextField);
		this.panel.add(quantityLabel);
		this.panel.add(quatityTextField);
		this.panel.add(sellButton);
		this.panel.repaint();
		this.panel.revalidate();
	}

	
	public void sellListener(ActionListener listenForButton) {
		sellButton.addActionListener(listenForButton);
	}
	
	public void searchListener(ActionListener listenForButton) {
		searchButton.addActionListener(listenForButton);
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(null, errorMessage);
	}
}
