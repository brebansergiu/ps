package client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

import controller.HomeController;

public class Client extends Thread {
    private static final int PORT=9999;
    private SocketChannel SChan;
    private String ServerName;
    private boolean notifyServer = false;
    private ByteBuffer writeBuffer;
    
    public Client(){
    	writeBuffer=ByteBuffer.allocateDirect(300);
    	
    }
    public void run() {
         
        ServerName = "breban-X550CL";
        
        Connect(ServerName);
        
         
    }
    
    public void Connect(String hostname) {
        try {
            InetAddress addr = InetAddress.getByName(hostname);
            SChan = SocketChannel.open(new InetSocketAddress(addr, PORT));
            SChan.configureBlocking(false);
            
            new HomeController(this);
        }
         
        catch (Exception e) {
        	System.out.print("Server inchis");
        }
    }
    
    public boolean observedAction(){
    	return notifyServer;
    }
    public void notifyServer(String message){
    	writeBuffer.clear();
        writeBuffer.put(message.getBytes());
        writeBuffer.flip();
        
    	try {
			SChan.write(writeBuffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	public boolean isNotifyServer() {
		return notifyServer;
	}

	public void setNotifyServer(boolean notifyServer) {
		this.notifyServer = notifyServer;
	}

	public static void main(String[] args) {
		Client client = new Client();
		client.run();
	}
     
}