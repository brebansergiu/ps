package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;

import org.xml.sax.SAXException;

import client.Client;

import model.Book;
import model.User;

import dbOperations.BookOperations;
import dbOperations.UserDBOperations;
import view.AdminView;

public class AdminController {

	private AdminView adminView;
	private BookOperations bookOperation;
	private UserDBOperations userOperations;
	
	private ArrayList<Book> books;
	private ArrayList<User> users;
	private Client client;
	
	public AdminController(User user, Client client){
		this.client = client;
		this.adminView = new AdminView();
		this.bookOperation = new BookOperations();
		this.userOperations = new UserDBOperations();
		this.users = userOperations.readUsersXML();
		this.books = bookOperation.readBookXML();
		this.adminView.setWelcome(new JLabel("welcome to your accound " + user.getUsername()));
		
		if(this.adminView.getOption1().isSelected()){
			this.adminView.setUsersTable(this.adminView.buildUsersTable(users));
		}else{
			this.adminView.setBooksTable(this.adminView.buildBooksTable(books));
		}
		if(adminView.getOptionCreate().isSelected()){
			adminView.setCRUDPanel("create");
		}else if(adminView.getOptionUpdate().isSelected()){
			adminView.setCRUDPanel("update");
		}else if(adminView.getOptionDelete().isSelected()){
			adminView.setCRUDPanel("delete");
		}
		
		setSwitchModelButtonListener();
		setSwitchCRUDButton();
		setExecuteButton();
		
		
	}
	
	public void setSwitchModelButtonListener(){
			this.adminView.switchModelButtonListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(adminView.getOption1().isSelected()){
					adminView.setUsersTable(adminView.buildUsersTable(users));
				}else{
					adminView.setBooksTable(adminView.buildBooksTable(books));
					setReportButtonListener();
				}
				
				if(adminView.getOptionCreate().isSelected()){
					adminView.setCRUDPanel("create");
				}else if(adminView.getOptionUpdate().isSelected()){
					adminView.setCRUDPanel("update");
				}else if(adminView.getOptionDelete().isSelected()){
					adminView.setCRUDPanel("delete");
				}
				
				adminView.repaintPanel();
				
				/*setSwitchModelButtonListener();
				setSwitchCRUDButton();
				setExecuteButton();*/
				
			}
		});
	}
	
	public void setReportButtonListener(){
		this.adminView.reportButtonListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				bookOperation.writeReport(books);
				
			}
		});
	}
	
	public void setSwitchCRUDButton(){
		this.adminView.switchCRUDButtonListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(adminView.getOption1().isSelected()){
					adminView.setUsersTable(adminView.buildUsersTable(users));
				}else{
					adminView.setBooksTable(adminView.buildBooksTable(books));
					setReportButtonListener();
				}
				
				if(adminView.getOptionCreate().isSelected()){
					adminView.setCRUDPanel("create");
				}else if(adminView.getOptionUpdate().isSelected()){
					adminView.setCRUDPanel("update");
				}else if(adminView.getOptionDelete().isSelected()){
					adminView.setCRUDPanel("delete");
				}
				
				adminView.repaintPanel();
				
				/*setSwitchModelButtonListener();
				setSwitchCRUDButton();
				setExecuteButton();*/
				
			}
		});
	}
	
	public void setExecuteButton(){
		this.adminView.executeButtonListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(adminView.getOption1().isSelected()){
					if( adminView.getOptionCreate().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						String firstName = adminView.getFirstNameField().getText();
						String lastName = adminView.getLastNameField().getText();
						String username = adminView.getUsernameField().getText();
						String password = adminView.getPasswordField().getText();
						boolean is_staff = (adminView.getIs_staffField().getText().equals("true"))? true:false;
						String email = adminView.getEmailField().getText();
						
						
						for(User i: users){
							if (i.getId() == id){
								adminView.displayErrorMessage("Exista un user cu acest id");
								return;
							}
						}
						
						User user = new User(id, firstName, lastName, username, password, is_staff, email);
						
						users.add(user);
						try {
							userOperations.createUser(user);
							client.notifyServer("user");
						} catch (SAXException e1) {
							e1.printStackTrace();
						}
						
						adminView.setUsersTable(adminView.buildUsersTable(users));
						adminView.setCRUDPanel("create");
						adminView.repaintPanel();
						
					}else if(adminView.getOptionUpdate().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						String firstName = adminView.getFirstNameField().getText();
						String lastName = adminView.getLastNameField().getText();
						String username = adminView.getUsernameField().getText();
						String password = adminView.getPasswordField().getText();
						boolean is_staff = (adminView.getIs_staffField().getText().equals("true"))? true:false;
						String email = adminView.getEmailField().getText();
						
						User user = new User(id, firstName, lastName, username, password, is_staff, email);
						
						int i=0;
						for(i=0; i<users.size() ; i++){
							if (users.get(i).getId() == id){
								users.remove(i);
								users.add(i, user);
								//TODO: create user update operation and update xml
								client.notifyServer("user");
								adminView.setUsersTable(adminView.buildUsersTable(users));
								adminView.setCRUDPanel("update");
								adminView.repaintPanel();
								return;
							}
						}
						adminView.displayErrorMessage("Nu exista un user cu acest id");
						
						
					}else if(adminView.getOptionDelete().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						int i=0;
						for(i=0; i<users.size(); i++){
							if (users.get(i).getId() == id){
								users.remove(i);
								client.notifyServer("user");
								adminView.setUsersTable(adminView.buildUsersTable(users));
								adminView.setCRUDPanel("delete");
								adminView.repaintPanel();
								return;
							}
						}
					}
				}else{
					if( adminView.getOptionCreate().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						String title = adminView.getTitleField().getText();
						String author = adminView.getAuthorField().getText();
						int year = Integer.parseInt(adminView.getYearField().getText());
						int quantity = Integer.parseInt(adminView.getQuantityField().getText());
						int price = Integer.parseInt(adminView.getPriceField().getText());
						
						for(Book i: books){
							if (i.getId() == id){
								adminView.displayErrorMessage("Exista o carte cu acest id");
								return;
							}
						}
						Book book = new Book(id, title, author, quantity, price, year);
						books.add(book);
						
						try {
							bookOperation.insertBook(book);
							client.notifyServer("book");
						} catch (SAXException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						adminView.setUsersTable(adminView.buildBooksTable(books));
						adminView.setCRUDPanel("create");
						adminView.repaintPanel();
						
					}else if(adminView.getOptionUpdate().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						String title = adminView.getTitleField().getText();
						String author = adminView.getAuthorField().getText();
						int year = Integer.parseInt(adminView.getYearField().getText());
						int quantity = Integer.parseInt(adminView.getQuantityField().getText());
						int price = Integer.parseInt(adminView.getPriceField().getText());
						Book book = new Book(id, title, author, quantity, price, year);
						
						int i=0;
						for(i=0;i<books.size();i++){
							if (books.get(i).getId() == id){
								books.remove(i);
								books.add(i, book);
							}
						}
						
						bookOperation.updateBook(book);
						client.notifyServer("book");
						
						adminView.setBooksTable(adminView.buildBooksTable(books));
						adminView.setCRUDPanel("update");
						adminView.repaintPanel();
						
					}else if(adminView.getOptionDelete().isSelected()){
						int id = Integer.parseInt(adminView.getIdField().getText());
						
						int i=0;
						for(i=0;i<books.size();i++){
							if (books.get(i).getId() == id){
								books.remove(i);
							}
						}
						
						bookOperation.deleteBook(id);
						
						client.notifyServer("book");
						adminView.setBooksTable(adminView.buildBooksTable(books));
						adminView.setCRUDPanel("delete");
						adminView.repaintPanel();
					}
					
				}
				
				
			}
		});
		
		
	}
}
