package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import client.Client;

import dbOperations.BookOperations;
import model.Book;
import model.Store;
import model.User;
import view.UserView;

public class UserController {

	private User user;
	private UserView userView;
	private BookOperations bookOperations;
	private ArrayList<Book> searchedBooks;
	private Client client;
	
	public UserController(User user,Client client){
		this.client = client;
		this.user = user;
		this.bookOperations = new BookOperations();
		this.userView = new UserView(bookOperations);
		this.userView.setWelcomeText(user.getUsername());
		searchedBooks = bookOperations.readBookXML();
		
		this.userView.setBooksTable(userView.buildBooksTable(searchedBooks));
		
		this.userView.searchListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				searchedBooks = bookOperations.searchBooks(userView.getSearchTextField().getText());
				userView.setBooksTable(userView.buildBooksTable(searchedBooks));
				setSellListener();
			}
		});
		
		this.setSellListener();
	}
	
	public void setSellListener(){
		this.userView.sellListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int bookId = Integer.parseInt(userView.getIdTextField().getText());
				int bookQuantity = Integer.parseInt(userView.getQuatityTextField().getText());
				if (bookQuantity > 0){
					for (Book book:searchedBooks){
						if (book.getId() == bookId){
							if((book.getQuantity() - bookQuantity) >= 0){
								book.setQuantity(book.getQuantity() - bookQuantity);
								userView.setBooksTable(userView.buildBooksTable(searchedBooks));
								bookOperations.updateBook(book);
								client.notifyServer("user");
								setSellListener();
							}else{
								userView.displayErrorMessage("Nu sunt destule carti in stoc");
							}
						}
					}
				}else{
					userView.displayErrorMessage("Introdu o valoare mai mare ca 0");
				}
				
			}
		});
	}
	
}
