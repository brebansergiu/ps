package server;

	 
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import observers.ClientObserver;
	
public class ServerFrame extends JFrame{


    private JTextArea historyBox=new JTextArea(10,45);
    private JScrollPane historyScrollPane=new JScrollPane(historyBox,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

    private JButton Start = new JButton("Start Server!");
    private Server ChatServer;
    private InetAddress ServerAddress ;
     
    public ServerFrame() {
        setTitle("Server");
        setSize(560,400);
        Container cp=getContentPane();
        cp.setLayout(new FlowLayout());
        cp.add(new JLabel("Server History"));
        cp.add(historyScrollPane);
        cp.add(Start);
        
        Start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ChatServer = new Server();
                ChatServer.start();
            }
        });
         
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
	   }
	     
    public static void main(String[] args) {
        // TODO code application logic here
        new ServerFrame();
    }
     
    public class Server extends Thread {
        private static final int PORT=9999;
        private LinkedList Clients;
        public  ServerSocketChannel SSChan;
        private CharsetDecoder asciiDecoder;
        private ByteBuffer ReadBuffer;
        private ByteBuffer WriteBuffer;
        private Selector ReaderSelector;
        
        public Server() {
            Clients=new LinkedList();
            ReadBuffer=ByteBuffer.allocateDirect(300);
            asciiDecoder = Charset.forName( "US-ASCII").newDecoder();
        }
         
        public void InitServer() {
            try {
                SSChan=ServerSocketChannel.open();
                SSChan.configureBlocking(false);
                ServerAddress=InetAddress.getLocalHost();
                System.out.println(ServerAddress.toString());
                 
                SSChan.socket().bind(new InetSocketAddress(ServerAddress,PORT));
                 
                ReaderSelector=Selector.open();
                historyBox.append(ServerAddress.getHostName()+"<Server> Started. \n");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        public void run() {
            InitServer();
             
            while(true) {
                acceptNewConnection();
                readMassage();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
         
        public void acceptNewConnection() {
            SocketChannel newClient;
            try {
                 
                while ((newClient = SSChan.accept()) != null) {
                    ChatServer.addClient(newClient);
                    historyBox.append("<------welcome you :" + newClient.getLocalAddress() + "------->");
                    
                }
                 
            } catch (IOException e) {
                e.printStackTrace();
            }
             
        }
         
        public void addClient(SocketChannel newClient) {
            Clients.add(newClient);
            try {
                newClient.configureBlocking(false);
          
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        public void readMassage() {
            try {
                 
                ReaderSelector.selectNow();
                Set readkeys=ReaderSelector.selectedKeys();
                Iterator iter=readkeys.iterator();
                while(iter.hasNext()) {
                    SelectionKey key=(SelectionKey) iter.next();
                    iter.remove();
                     
                    SocketChannel client=(SocketChannel)key.channel();
                    ReadBuffer.clear();
                     
                    long num=client.read(ReadBuffer);
                     
                    
                         
                        StringBuffer str=(StringBuffer)key.attachment();
                        ReadBuffer.flip();
                        String data= asciiDecoder.decode(ReadBuffer).toString();
                        ReadBuffer.clear();
                         
                        str.append(data);
                         
                        String line = str.toString();
                        System.out.print(line + "primesc");
                    }
                
                 
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.print("eror");
            }
        
    }
        public void updateClients(){
        	System.out.print("sunt notificat");
        }
        
    }
}

