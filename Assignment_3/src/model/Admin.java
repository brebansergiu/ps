package model;

public class Admin {
	public String name;
	public String password;
	
	public Admin(){
		
	}
	
	public Admin(String name, String password){
		this.name = name;
		this.password = password;
	}
	
	public Admin createNewAdminAccount(String name, String password){
		return new Admin(name,password);
	}
}
