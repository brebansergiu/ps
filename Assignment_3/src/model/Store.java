package model;

import java.util.ArrayList;
import java.util.List;

public class Store {

	private ArrayList<Book> books;
	
	public Store(){
		this.books = new ArrayList<Book>();
	}	

	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

}
