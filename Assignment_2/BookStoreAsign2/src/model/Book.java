package model;

public class Book {
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	private String title;
	private String author;
	private int quantity;
	private int price;
	private int year;
	
	public Book(int id, String title, String author,int quantity, int price, int year){
		this.id = id;
		this.title = title;
		this.author = author;
		this.quantity = quantity;
		this.price = price;
		this.year = year;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public String getAttr(String attr){
		switch (attr) {
        case "title": return this.getTitle();
		case "author": return this.getAuthor();
        case "quantity": return "" + this.getQuantity();
        case "price": return "" + this.getPrice();
        case "year": return "" + this.getYear();
		}
		return null;
	}

}
