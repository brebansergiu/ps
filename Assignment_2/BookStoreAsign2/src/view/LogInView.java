package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import javax.swing.border.EmptyBorder;

import model.Store;
import model.User;

import controller.AdminController;
import controller.UserController;



public class LogInView extends JFrame{

	private JPanel panel;
	JButton newAccount;
	JLabel labelName;
	JTextField nameField;
	JLabel password;
	JTextField passwordField;
	JButton logIn;
	
	public LogInView(){
		//this.setResizable(false);
		this.panel = new JPanel();
		this.newAccount = new JButton("New Account");
		this.labelName = new JLabel("Name");
		this.nameField = new JTextField("",15);
		this.password = new JLabel("Password");
		this.passwordField = new JPasswordField("",15);
		this.logIn = new JButton("Log In");
		
		this.add(panel);
		//this.bank=Bank.getInstance();
		
		this.setTitle("BookStore");

		this.setSize(200, 200);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		createLogInPanel();
		//this.add(panel);
		this.setVisible(true);
	}
	

	private void createLogInPanel(){
		panel.add(labelName);
		panel.add(nameField);
		panel.add(password);
		panel.add(passwordField);
		panel.add(logIn);
		panel.add(newAccount);
	}

	public void loginListener(ActionListener listenForButton) {
		logIn.addActionListener(listenForButton);
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(null, errorMessage);
	}
	
	public JTextField getNameField() {
		return nameField;
	}
	
	public void setNameField(JTextField nameField) {
		this.nameField = nameField;
	}
	
	public JTextField getPasswordField() {
		return passwordField;
	}
	
	public void setPasswordField(JTextField passwordField) {
		this.passwordField = passwordField;
	}
	

	public void registerListener(ActionListener listenForButton) {
		newAccount.addActionListener(listenForButton);
	}
	
	public void userLogIn(User user){
		this.setVisible(false);
		new UserController(user);
	}
	
	public void adminLogIn(User user){
		this.setVisible(false);
		new AdminController(user);
	}
	
}
