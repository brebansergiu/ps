package view;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.HomeController;

public class RegisterView extends JFrame{
	
	private JPanel contentPane;
	private JTextField tf_firstname;
	private JTextField tf_lastname;
	private JTextField tf_username;
	private JPasswordField tf_password;
	private JTextField tf_email;
	private JLabel lblFirstname;
	private JLabel lblNewLabel;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JLabel lblEmail;
	private JButton btnRegister;


	public RegisterView() {
		this.contentPane = new JPanel();
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 645, 379);
		this.setBackground(Color.BLUE);
		this.setResizable(false);
		
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(null);
		this.setContentPane(this.contentPane);
		
		this.lblFirstname = new JLabel("Firstname:");
		this.lblFirstname.setBounds(94, 49, 86, 20);
		this.contentPane.add(this.lblFirstname);
		
		this.tf_firstname = new JTextField();
		this.tf_firstname.setBounds(225, 49, 171, 20);
		this.contentPane.add(this.tf_firstname);
		this.tf_firstname.setColumns(10);
		
		this.lblNewLabel = new JLabel("Lastname:");
		this.lblNewLabel.setBounds(94, 96, 88, 17);
		this.contentPane.add(this.lblNewLabel);
		
		this.lblUsername = new JLabel("Username:");
		this.lblUsername.setBounds(94, 144, 80, 14);
		this.contentPane.add(this.lblUsername);
		
		this.lblPassword = new JLabel("Password:");
		this.lblPassword.setBounds(94, 180, 80, 20);
		this.contentPane.add(this.lblPassword);
		
		this.lblEmail = new JLabel("Email:");
		this.lblEmail.setBounds(94, 225, 58, 20);
		this.contentPane.add(this.lblEmail);
		
		this.tf_lastname = new JTextField();
		this.tf_lastname.setBounds(225, 94, 171, 20);
		this.contentPane.add(this.tf_lastname);
		this.tf_lastname.setColumns(10);
		
		this.tf_username = new JTextField();
		this.tf_username.setBounds(225, 141, 171, 20);
		this.contentPane.add(this.tf_username);
		this.tf_username.setColumns(10);
		
		this.tf_password = new JPasswordField();
		this.tf_password.setBounds(225, 180, 171, 20);
		this.contentPane.add(this.tf_password);
		this.tf_password.setColumns(10);
		
		this.tf_email = new JTextField();
		this.tf_email.setBounds(225, 225, 171, 20);
		this.contentPane.add(this.tf_email);
		this.tf_email.setColumns(10);
		
		this.btnRegister = new JButton("Register");
		this.btnRegister.setBounds(94, 270, 112, 28);
		this.contentPane.add(this.btnRegister);
	}

	public void setVisibleFrame(Boolean visible){
		this.setVisible(visible);
	}

	
	public JTextField getTf_firstname() {
		return tf_firstname;
	}

	public JTextField getTf_lastname() {
		return tf_lastname;
	}

	public JTextField getTf_username() {
		return tf_username;
	}

	public JPasswordField getTf_password() {
		return tf_password;
	}

	public JTextField getTf_email() {
		return tf_email;
	}

	public void register(){
		this.setVisible(false);
		new HomeController();
	}
	
	public void registerListener(ActionListener listenForButton) {
		btnRegister.addActionListener(listenForButton);
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(null, errorMessage);
	}
}
