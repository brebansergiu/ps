package dbOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.User;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;


public class UserDBOperations {

	public ArrayList<User> readUsersXML() {
		ArrayList<User> listUsers = new ArrayList<>();
		try {
			String filePath = "src/xml/user.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(fileInputStream);

			Element users = doc.getRootElement();
			List<Element> userElement = users.getChildren();

			for (int i = 0; i < userElement.size(); i++) {
				Element user = userElement.get(i);
				
				Integer id = user.getAttribute("id").getIntValue();
				List<Element> userFields = user.getChildren();
				String firstName = userFields.get(0).getValue();
				String lastName = userFields.get(1).getValue();
				String username = userFields.get(2).getValue();
				String password = userFields.get(3).getValue();
				boolean is_staff = (userFields.get(4).getValue().equals("true")) ? true : false;
				String email = userFields.get(5).getValue();
				
			
				User newUser = new User(id, firstName, lastName, username, password, is_staff, email);
				
				listUsers.add(newUser);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return listUsers;
	}
	
	
	public User authentication(String nume, String parola) {
		User user = null;
		try {
			String filePath = "src/xml/user.xml";
			String absolutePath = new File(filePath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();

			Document doc = saxBuilder.build(fileInputStream);
			Element elementXml = doc.getRootElement();
			List<Element> xmlUsers = elementXml.getChildren();
			
			for (int i = 0; i < xmlUsers.size(); i++) {
				Element userElement = xmlUsers.get(i);
				List<Element> element = userElement.getChildren();
				for (int j = 0; j < element.size()-1; j++) {
					if ("username".equals(element.get(j).getName()) && element.get(j).getValue().equals(nume)
							&& "password".equals(element.get(j+1).getName()) && element.get(j+1).getValue().equals(parola)){
						
						//Element userElement = staff;
						// obtinere elemente
						Integer id = userElement.getAttribute("id").getIntValue();
						List<Element> fields = userElement.getChildren();
						String firstname = fields.get(0).getValue();
						String lastname = fields.get(1).getValue();
						String username = fields.get(2).getValue();
						String password = fields.get(3).getValue();
						boolean is_staff = fields.get(4).getValue().equals("true") ? true : false;
						String email = fields.get(5).getValue();

						user = new User(id, firstname, lastname, username, password, is_staff, email);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.print("1");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.print("2");
		} catch (JDOMException e) {
			e.printStackTrace();
			System.out.print("3");
		}
		return user;
	}
	
	public void createUser(User user) throws SAXException{
		try {

			String filepath = "src/xml/user.xml";
			String absolutePath = new File(filepath).getAbsolutePath();
			System.out.print(absolutePath);
			File xmlFile = new File(absolutePath);
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
		
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(fileInputStream);

			Element element = document.getRootElement();
			Element userField = new Element("user");
			userField.setAttribute("id", user.getId() + "");

			Element firstname = new Element("firstname");
			firstname.addContent(new Text(user.getFirstname()));
			Element lastname = new Element("lastname");
			lastname.addContent(new Text(user.getLastname()));
			Element username = new Element("username");
			username.addContent(new Text(user.getUsername()));
			Element password = new Element("password");
			password.addContent(new Text(user.getPassword()));
			Element is_staff = new Element("is_staff");
			is_staff.addContent(new Text( (user.getIs_staff()) ? "true" : "false"  ));
			Element email = new Element("email");
			email.addContent(new Text(user.getEmail()));

			userField.addContent(firstname);
			userField.addContent(lastname);
			userField.addContent(username);
			userField.addContent(password);
			userField.addContent(is_staff);
			userField.addContent(email);
			element.addContent(userField);

			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(document, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteUser(User user) throws SAXException{
		try {

			String filepath = "src/xml/user.xml";
			String absolutePath = new File(filepath).getAbsolutePath();
			System.out.print(absolutePath);
			
			File xmlFile = new File(absolutePath);
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
		
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(fileInputStream);

			Element users = document.getRootElement();
			List<Element> userXml = users.getChildren();
			for (int i = 0; i < userXml.size() - 1; i++) {
				Element userElement = userXml.get(i);
				Integer id = userElement.getAttribute("id").getIntValue();
				if (id.equals(user.getId())) {
					
					for (int j = 0; j < userXml.size()-1; j++) {
						if (!userXml.get(j).getValue().equals(user.getAttr(userXml.get(j).getName()))){
							userXml.get(j).removeContent();
							userXml.get(j).addContent( user.getAttr(userXml.get(j).getName()) );
						}
					}
					
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(document, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	
	public void deleteUser(int id) {
		try {
			String filepath = "src/xml/user.xml";
			String absolutePath = new File(filepath).getAbsolutePath();
			File xmlFile = new File(absolutePath);
			FileInputStream fileInputStream = new FileInputStream(xmlFile);
			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(fileInputStream);

			Element users = doc.getRootElement();
			List<Element> userXml = users.getChildren();
			for (int i = 0; i < userXml.size() - 1; i++) {
				Element userElement = userXml.get(i);
				Integer idd = userElement.getAttribute("id").getIntValue();
				if (idd.equals(id)) {
					users.removeContent(userElement);
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
}
