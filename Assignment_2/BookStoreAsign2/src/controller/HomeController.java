package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.SAXException;

import dbOperations.UserDBOperations;

import model.Store;
import model.User;

import view.LogInView;
import view.RegisterView;



public class HomeController {
	

	private LogInView loginView;
	private RegisterView registerView;

	private TreeMap<String, User> users;
	private Store store;
	
	private UserDBOperations xmlDBOperations;
	
	public HomeController(){
		this.users = new TreeMap<String, User>();
		this.store = new Store();
		this.xmlDBOperations = new UserDBOperations();
		
		this.loginView = new LogInView();
		this.registerView = new RegisterView();
		
		this.loginView.loginListener(new LoginListener());
		this.loginView.registerListener(new OpenRegistrationListener());
		this.registerView.registerListener(new RegisterListener());
	}
	
	class LoginListener implements ActionListener{ 
        public void actionPerformed(ActionEvent e) { 
            String username;
            String password;
            
            if (loginView.getNameField().getText().equals("") ||
            	loginView.getPasswordField().getText().equals("") ){
            	
            		loginView.displayErrorMessage("Campurile sunt necesare pentru autentificare"); 
            	
            }
            	
            	username = loginView.getNameField().getText();
            	password = loginView.getPasswordField().getText();
            	User user = xmlDBOperations.authentication(username, password);
            	if (user != null){
            		if (user.getIs_staff()){
            			 loginView.adminLogIn(user);
            		}else{
            			  loginView.userLogIn(user);
            		}
            	}else{
             	   loginView.displayErrorMessage("Nu exista acest user sau datele sunt completate gresit"); 
                }
        }
    }
	
	class OpenRegistrationListener implements ActionListener{ 
        public void actionPerformed(ActionEvent e) { 
        	registerView.setVisibleFrame(true);  
        }  
    }
	
	class RegisterListener implements ActionListener{ 
        public void actionPerformed(ActionEvent e) { 
        	String firstname;
        	String lastname;
            String username;
            String password;
            String email;
            boolean is_staff = false;

            if (registerView.getTf_firstname().getText().equals("") ||
        		registerView.getTf_lastname().getText().equals("") ||
        		registerView.getTf_username().getText().equals("") ||
        		registerView.getTf_password().getText().equals("") ||
        		registerView.getTf_email().getText().equals("")){
            	
	            	  loginView.displayErrorMessage("Trebuie introduse date valide"); 
	            	  return;
	            	  
            }
            
            	firstname = registerView.getTf_firstname().getText();
            	lastname = registerView.getTf_lastname().getText();
            	username = registerView.getTf_username().getText();
            	password = registerView.getTf_password().getText();
            	email = registerView.getTf_email().getText();
          
            	User user = new User(firstname, lastname, username, password, is_staff, email);
            	try {
					xmlDBOperations.createUser(user);
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
            	registerView.setVisibleFrame(false);
                System.out.print("lalalo");
  
              

        }  
    }
	

}

