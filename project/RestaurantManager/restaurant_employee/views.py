from django.shortcuts import render

from django.views.generic import *
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from restaurant_customer.models import Order
from form import *
# Create your views here.


class SignInView( FormView ):
	template_name = 'restaurant_employee/sign-in.html'
	form_class = SignInForm
	success_url = reverse_lazy('account-employee')

	def dispatch(self, request, *args, **kwargs):

		if request.user.is_authenticated():
			return redirect( 'account-employee' )

		return super( SignInView, self ).dispatch( request, *args, **kwargs )

	def post( self, request, *args, **kwargs ):

		form = self.get_form()

		if( form.is_valid() ):
			if form.get_user() is not None:
				login( request, form.get_user() )

		return super( SignInView, self ).post( self, request, *args, **kwargs )



class AccountView(ListView):
	template_name = 'restaurant_employee/orderlist.html'
	model = Order


class UpdateOrderView(UpdateView):
	template_name = 'restaurant_employee/update-order.html'
	form_class = UpdateOrderForm
	success_url = reverse_lazy('account-employee')

	def get_object( self, queryset = None ):
		try:
			return Order.objects.get( pk = self.kwargs['pk'])
		except Order.DoesNotExist:
			raise Http404