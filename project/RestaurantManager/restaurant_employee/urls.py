
from django.conf.urls import include, url
from django.contrib import admin
from restaurant_employee.views import *
from django.contrib.admin.views.decorators import staff_member_required

urlpatterns = [
	url(r'^account/order-detail/(?P<pk>\d+)/', staff_member_required(UpdateOrderView.as_view()), name = 'order-detail' ),
	url(r'^account/', staff_member_required(AccountView.as_view()), name = 'account-employee' ),
	url(r'^$', SignInView.as_view(), name = 'sign-in-employee' ),
	
]
