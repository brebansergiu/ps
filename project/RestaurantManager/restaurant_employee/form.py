from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User

from django.contrib.auth.forms import AuthenticationForm

from restaurant_customer.models import *

class SignInForm(AuthenticationForm):

	error_messages = {
						'invalid_login': ( u'Datele de autentificare sunt incorecte.' )
					}


	def __init__(self, *args, **kwargs):
		self.base_fields['username'].label = 'Email'
		self.base_fields['username'].widget.attrs['class'] = 'form-control'
		self.base_fields['username'].widget.attrs['placeholder'] = ( u'Introdu adresa de email' )

		self.base_fields['password'].widget.attrs['class'] = 'form-control'
		self.base_fields['password'].widget.attrs['placeholder'] = ( u'Introdu parola' )

		super( SignInForm, self ).__init__( *args, **kwargs )


class UpdateOrderForm(ModelForm):
	class Meta:
		model = Order
		fields = {'orderTime', 'delivered'}
