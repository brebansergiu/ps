$(document).ready(function(){

   initialize();
    
});


function initialize() {
  var mapCanvas = document.getElementById('map');
  var mapOptions = {
    center: new google.maps.LatLng(46.77215, 23.585334999999986),
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(mapCanvas, mapOptions)

  google.maps.event.addDomListener(window, 'load', initialize);
}
