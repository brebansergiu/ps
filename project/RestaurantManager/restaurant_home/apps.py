from __future__ import unicode_literals

from django.apps import AppConfig


class RestaurantHomeConfig(AppConfig):
    name = 'restaurant_home'
