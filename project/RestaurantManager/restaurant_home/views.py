from django.shortcuts import render

from django.shortcuts import redirect
from django.views.generic import TemplateView
# Create your views here.


class HomeView(TemplateView):
	template_name = 'index.html'
	
	def dispatch(self, request, *args, **kwargs):
		if self.request.user.is_authenticated():
			if self.request.user.is_superuser:
				return redirect( 'admin:index' )
			return redirect( 'order' )
		
		return super( HomeView, self).dispatch( request, *args, **kwargs )