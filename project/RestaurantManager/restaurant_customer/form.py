from django.forms import ModelForm
from restaurant_customer.models import *
from django import forms

class OrderForm(forms.Form):
	foodItems = forms.ModelMultipleChoiceField(queryset=FoodItem.objects.all(),widget=forms.CheckboxSelectMultiple())
	address = forms.CharField()
	details = forms.CharField()