from tastypie.resources import ModelResource
from restaurant_customer.models import Order
from restaurant_customer.authorizations import OrderAuthorization
from tastypie.authorization import Authorization

class OrderResource(ModelResource):
	class Meta:
		queryset = Order.objects.all()
		resource_name = 'order'
		authorization = OrderAuthorization()
		list_allowed_methods = ['get']
		detail_allowed_methods = ['put']

