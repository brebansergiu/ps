from django.contrib import admin
from restaurant_customer.models import *

admin.site.register(FoodItem)
admin.site.register(Order)