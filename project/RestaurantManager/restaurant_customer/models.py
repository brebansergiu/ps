from __future__ import unicode_literals

from django.db import models
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class FoodItem( models.Model ):
	id = models.AutoField( primary_key = True )
	name = models.CharField( max_length = 50, default = '' )
	description = models.CharField(max_length = 500, default = '')
	price = models.IntegerField()
	
	def __unicode__(self):
		separator = " - "
		return separator.join([self.name, self.description, str(self.price)])

class Order(models.Model):
	id = models.AutoField( primary_key = True )
	address = models.CharField(max_length = 250, default = '')
	details = models.CharField(max_length = 250, default = '')
	orderTime = models.DateTimeField()
	delivered = models.BooleanField(default = False)
	foodItems = models.ManyToManyField(FoodItem)
	user = models.ForeignKey(User)