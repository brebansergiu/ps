from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized


class OrderAuthorization(Authorization):
    def is_user_anonymous(self, bundle):
        loggedUser = bundle.request.user
        return loggedUser.is_anonymous()

    def read_list(self, object_list, bundle):
        if self.is_user_anonymous(bundle):
            return False

        if bundle.request.user.is_staff:
            return object_list
        else:
            return False

    def read_detail(self, object_list, bundle):
        if self.is_user_anonymous(bundle):
            return False

        if bundle.request.user.is_staff:
            return True
        else:
            return False

    def create_list(self, object_list, bundle):
        raise Unauthorized()

    def create_detail(self, object_list, bundle):
        raise Unauthorized()

    def update_list(self, object_list, bundle):
        raise Unauthorized()

    def update_detail(self, object_list, bundle):
        if self.is_user_anonymous(bundle):
            return False

        if bundle.request.user.is_staff:
            return True
        else:
            return False

    def delete_list(self, object_list, bundle):
        raise Unauthorized()

    def delete_detail(self, object_list, bundle):
        raise Unauthorized()