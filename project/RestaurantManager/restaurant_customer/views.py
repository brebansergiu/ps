from django.shortcuts import render

from datetime import datetime, date, timedelta
from django.shortcuts import redirect
from django.views.generic import *
from form import *
# Create your views here.


class OrderView(FormView):
	template_name = 'restaurant_customer/order.html'
	form_class = OrderForm
	success_message = ('Comanda a fost realizata cu succes.')
	success_url = '/'

	def dispatch(self, request, *args, **kwargs):

		if not request.user.is_authenticated():
			return redirect( 'home' )

		return super( OrderView, self ).dispatch( request, *args, **kwargs )

	def post( self, request, *args, **kwargs ):
		form = self.get_form()

		if form.is_valid():
			order = Order()
			order.orderTime = datetime.now() + timedelta( hours = 1 )
			order.adrress = form.cleaned_data['address']
			order.details = form.cleaned_data['details']
			order.user_id = request.user.pk
			order.save()

			items_ids = [item.pk for item in form.cleaned_data['foodItems']]
			items = FoodItem.objects.filter(pk__in = items_ids)

			for item in items:
				order.foodItems.add(item)
			

		return super( OrderView, self ).post( self, request, *args, **kwargs )