from django import forms
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from models import Account
from models import Transaction

from django.contrib.auth.forms import AuthenticationForm


class SignInForm( AuthenticationForm ):

	error_messages = {
						'invalid_login': ( u'Datele de autentificare sunt incorecte.' )
					}


	def __init__(self, *args, **kwargs):
		self.base_fields['username'].label = 'Email'
		self.base_fields['username'].widget.attrs['class'] = 'form-control'
		self.base_fields['username'].widget.attrs['placeholder'] = ( u'Introdu adresa de email' )

		self.base_fields['password'].widget.attrs['class'] = 'form-control'
		self.base_fields['password'].widget.attrs['placeholder'] = ( u'Introdu parola' )

		super( SignInForm, self ).__init__( *args, **kwargs )


class RegisterUserForm( ModelForm ):

	class Meta:
		model = User

		fields = [ 'username', 'password' ]

		widgets = {
					'username': forms.TextInput( attrs = { 'required': True,
														'title': ( u'Choose a username' ),
														'placeholder': ( u'username' ),
														'class': 'form-control input-lg'
														} ),
					'password': forms.TextInput( attrs = { 'required': True,
														'placeholder': ( u'Password' ),
														'class': 'form-control input-lg'
														} )
				}

		labels = {
					'email': ( u'Chose a username' )
				}


	def is_valid( self ):

		valid = super( RegisterUserForm, self).is_valid()

		if not valid:
			return valid

		if User.objects.filter( username = self.cleaned_data['username'] ).exists():
			self.add_error( 'email', ValidationError( ( u'This user already exists' ), code='email_exists' ) )
			return False

		return True

#define account choices
account_choices = (
	('Saving account', 'Saving account'),
	('Spending account', 'Spending account'),
	)


class AccountForm(ModelForm):

	class Meta:
		model = Account


		exclude = ('user',)

		labels = {
					'account_type': (u'Account type'),
					'money':(u'Initial money')
		}

		widgets = {
				'money': forms.TextInput( attrs = { 'class': 'form-control ' } ),
		}


class TransactionForm(ModelForm):

	class Meta:
		model = Transaction

		fields = ['account_from', 'account_to', 'amount_of_money', 'details'] 

		widgets = {	
					'account_from': forms.TextInput( attrs = { 'required': True,
														'placeholder': ( u'Identification number' ),
														'class': 'form-control input-lg'
														} ),
					'account_to': forms.TextInput( attrs = { 'required': True,
														'placeholder': ( u'Identification number' ),
														'class': 'form-control input-lg'
														} ),
					'amount_of_money': forms.TextInput( attrs = { 'required': True,
														'placeholder': ( u'0000' ),
														'class': 'form-control input-lg'
														} ),
					'details': forms.TextInput( attrs = { 'required': False,
														'placeholder': ( u'Details' ),
														'class': 'form-control input-lg'
														} )
				}


class ReportForm(forms.Form):
	email = forms.EmailField(label='Your email', max_length=100)

	email.widget = forms.TextInput( attrs = { 'class': 'form-control' } )