# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-20 14:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0004_auto_20160319_1718'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount_of_money', models.FloatField()),
                ('details', models.CharField(max_length=200)),
                ('account_from', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='bank.Account')),
                ('account_to', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='bank.Account')),
            ],
        ),
    ]
