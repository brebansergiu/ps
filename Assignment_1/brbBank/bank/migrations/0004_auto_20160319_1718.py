# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-19 17:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0003_auto_20160319_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='account_type',
            field=models.CharField(choices=[('Saving account', 'Saving account'), ('Spending account', 'Spending account')], max_length=25),
        ),
    ]
