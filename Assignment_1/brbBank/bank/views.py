from django.shortcuts import render

from django.views.generic.edit import FormView
from django.views.generic import ListView
from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.db.models import Q

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from models import Account 
from models import Transaction

from form import SignInForm
from form import RegisterUserForm
from form import AccountForm
from form import TransactionForm
from form import ReportForm

import datetime

class SignInView( FormView ):
	template_name = 'sign-in.html'
	form_class = SignInForm
	success_url = 'account'

	def dispatch(self, request, *args, **kwargs):

		if request.user.is_authenticated():
			return redirect( 'account' )

		return super( SignInView, self ).dispatch( request, *args, **kwargs )

	def post( self, request, *args, **kwargs ):

		form = self.get_form()

		if( form.is_valid() ):
			if form.get_user() is not None:
				login( request, form.get_user() )
				
		try:
			request.POST['remember']
		except KeyError:
			self.request.session.set_expiry( 0 )

		return super( SignInView, self ).post( self, request, *args, **kwargs )



class RegisterView( FormView ):
	template_name = 'register.html'
	form_class = RegisterUserForm
	success_url =  'account' 


	def dispatch(self, request, *args, **kwargs):

		if request.user.is_authenticated():
			return redirect( 'account' )

		return super( RegisterView, self ).dispatch( request, *args, **kwargs )


	def post(self, request, *args, **kwargs):

		form = self.get_form()

		if form.is_valid():
			return self.form_valid( form )
		else:
			return self.form_invalid( form )


	def form_valid( self, form ):

		new_user = User.objects.create_user(	username	= form.cleaned_data['username'],
												password	= form.cleaned_data['password']
												)
		new_user.is_active = True
		new_user.save()

		return redirect( 'account' )

class AccountListView(ListView):
	template_name = 'index.html'
	model = Account

	def get_queryset(self):
		return Account.objects.filter( user = self.request.user.id )

	def get_context_data(self, **kwargs):

		context = super( AccountListView, self ).get_context_data( **kwargs )

		context['user'] = self.request.user

		return context

class AccountCreateView(SuccessMessageMixin, CreateView):
	template_name = "account-new.html"
	form_class = AccountForm
	success_message = ('The account has been created')

	def form_valid(self, form):

		new_account = Account()

		new_account.user = self.request.user
		new_account.account_type = form.cleaned_data['account_type']
		new_account.money = form.cleaned_data['money']
		new_account.date_created = datetime.datetime.now()

		new_account.save()

		return redirect('account')


class TransactionView(CreateView):
	template_name = 'transaction-new.html'
	form_class = TransactionForm
	success_url =  '/account' 

	def post(self, request, *args, **kwargs):

		form = self.get_form()

		if form.is_valid():
			account_from = form.cleaned_data['account_from']
			account_to =  form.cleaned_data['account_to']

			account_from.money = account_from.money - form.cleaned_data['amount_of_money']
			account_to.money = account_to.money + form.cleaned_data['amount_of_money']

			account_from.save()
			account_to.save()

		return self.form_valid(form)

	def form_valid(self, form):
		if (form.cleaned_data['account_from'].user != self.request.user):
			return redirect('transaction-new')

		return super( TransactionView, self ).form_valid( form )


class ReportView( FormView ):
	template_name = 'report-dialog.html'
	form_class = ReportForm
	success_message = 'Mesajul a fost trimis.'
	success_url = '/account'


	def form_valid(self, form):
		"""
		Overrides default form_valid to send email.
		"""
		accounts = Account.objects.filter( user = self.request.user)
		message = "You report\n"

		# for account in accounts:
		# 	transactions = Transaction.objects.filter(Q(account_from = account) | Q(account_to = account))
		# 	for transaction in transactions:
		# 		message = message + transaction.details

		EmailMessage( subject = 'Report bank', 
					body = message , 
					from_email = 'breban.sergiu@gmail.com', 
					to = [form.cleaned_data['email']]).send()
		# send_mail('Subject here',message, 'from@example.com',
		# 	[form.cleaned_data['email']], fail_silently=False)
		return super( ReportView, self ).form_valid( form )
