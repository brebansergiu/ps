from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

#define account choices
account_choices = (
	('Saving account', 'Saving account'),
	('Spending account', 'Spending account'),
	)

# Create your models here.
class Account(models.Model):
	account_type = models.CharField(max_length = 25,choices = account_choices )
	money = models.FloatField()
	date_created = models.DateField(auto_now = True)
	user = models.ForeignKey(User)

class Transaction(models.Model):
	account_from = models.ForeignKey(Account, related_name='+')
	account_to = models.ForeignKey(Account, related_name='+')
	amount_of_money = models.FloatField()
	details = models.CharField(max_length = 200)