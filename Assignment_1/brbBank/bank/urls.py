from django.conf.urls import url

from views import SignInView
from views import RegisterView
from views import AccountListView
from views import AccountCreateView
from views import TransactionView
from views import ReportView

urlpatterns = [
    url(r'^register/$', RegisterView.as_view(), name='register'),
	url(r'^account-new/$', AccountCreateView.as_view(), name='account-new'),
	url(r'^account/$', AccountListView.as_view(), name='account'),
    url(r'^transaction/$', TransactionView.as_view(), name='transaction-new'),
    url(r'^report-dialog/$', ReportView.as_view(), name='report-dialog'),
	url(r'^sign-out/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name = 'sign-out' ),

    url(r'^$', SignInView.as_view(), name='sign-in'),
]